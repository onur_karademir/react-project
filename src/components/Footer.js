import React from 'react';

function Footer () {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark p-3">
        <ul className="navbar-nav ml-auto mr-auto">
          <li className="nav-item active">
            <a className="nav-link" href="#">
              Home <span className="sr-only">(current)</span>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link" href="#">
              Link
            </a>
          </li>
          <li className="nav-item">
            <a
              className="nav-link"
              href="#"
              tabindex="-1"
              aria-disabled="true"
            >
              Disabled
            </a>
          </li>
        </ul>
       
    </nav>
    )
}

export default Footer;