import React from 'react';
import a from '../images/a.jpg';
const Contact = () => {
    return ( 
        <div className="bg-primary p-5">
        <div className="container">
          <div className="row">
          <div className="col-md-12">
            <h3 className="text-light text-center">Contact Me</h3>
          </div>
          <div className="col-md-12">
            <img src={a} class="img-responsive img-thumbnail" alt="Image"></img>
          </div>
          </div>
        </div>
      </div>
     );
}
 
export default Contact;