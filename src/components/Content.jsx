import React from 'react';
import c from '../images/c.jpg';
const Content = () => {
    return ( 
        <div className="bg-dark p-5">
        <div className="container">
          <div className="row">
          <div className="col-md-12">
            <h3 className="text-light text-center">Content</h3>
          </div>
          <div className="col-md-12">
            <img src={c} class="img-responsive img-thumbnail" alt="Image"></img>
          </div>
          </div>
        </div>
      </div>
     );
}
 
export default Content;