import React from 'react';
import b from '../images/b.jpg';
const About = () => {
    return ( 
        <div className="bg-secondary p-5">
        <div className="container">
          <div className="row">
          <div className="col-md-12">
            <h3 className="text-light text-center">About Me</h3>
          </div>
          <div className="col-md-12">
            <img src={b} class="img-responsive img-thumbnail" alt="Image"></img>
          </div>
          </div>
        </div>
      </div>
     );
}
 
export default About;