import React from "react";
import { NavLink } from "react-router-dom";
import {Switch,Redirect,Route } from 'react-router-dom';
const Header = () => {
  return (
    <nav className="navbar navbar-expand-md navbar-dark bg-dark">
      <NavLink exact className="navbar-brand" to="/">
        React Ana Sayfa
      </NavLink>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
          <li className="nav-item active">
            <NavLink exact className="nav-link" to="/content">
              Content
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink exact className="nav-item nav-link" to="/about">
              About
            </NavLink>
          </li>
          <li className="nav-item">
            <NavLink exact className="nav-item nav-link" to="/contact">
              Contact
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Header;
