import React from "react";
import a from '../images/a.jpg';
function Section() {
  return (
    <div className="bg-primary p-5">
      <div className="container">
        <div className="row">
        <div className="col-md-6">
          <h3 className="text-light">Where can I get some?</h3>
          <p className="text-light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
        </div>
        <div className="col-md-6">
          <img src={a} class="img-responsive img-thumbnail" alt="Image"></img>
        </div>
        </div>
      </div>
    </div>
    
  );
}

export default Section;
