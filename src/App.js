import React from "react";
import "./App.css";
import Navbar from "./components/Header";
import Section from "./components/Section";
import Footer from "./components/Footer";
import SectionMiddle from "./components/Sectionmiddle";
import Sectionbottom from "./components/Sectionbottom";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Content from "./components/Content";
import About from "./components/About";
import Contact from "./components/Contact";

function App() {
  return (
    <BrowserRouter>
      <Navbar></Navbar>
      <Switch>
        <Route path="/" exact component={Home}></Route>
        <Route path="/content" exact component={Content}></Route>
        <Route path="/about" exact component={About}></Route>
        <Route path="/contact" exact component={Contact}></Route>
      </Switch>
      <Redirect to="/"></Redirect>
    </BrowserRouter>
  );
}

const Home = () => {
  return (
    <div className="wrapper">
      <Section></Section>
      <SectionMiddle></SectionMiddle>
      <Sectionbottom></Sectionbottom>
      <Footer></Footer>
    </div>
  );
};

export default App;
